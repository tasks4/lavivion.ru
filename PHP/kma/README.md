Тестовое задание для KMA (version 2)
Можно использовать только библиотеку php-amqplib. Фреймворки запрещены.
Библиотеки для подключения к базам запрещены.
Все должно быть завернуто в docker для быстрого запуска и проверки вашего
решения.
Задание:
1) Создать массив из 10 URLs
2) Отправить по очереди каждый элемент массива с задержкой от 5 до 30 секунд в
   RabbitMq.
3) В очереди запросить этот url и получить длину контента. Сохранить их в
   MariaDB и ClickHouse. Допускаются репликации.
4) Сделать запрос с использованием PHP к MariaDB и ClickHouse на выборку.
   Сгруппировать по минуте все строки. Результат должен содержать следующие
   данные:
1) Вывести сколько строк за минуту
2) Вывести минуту группировки
3) Вывести среднюю длину контента
4) Вывести когда было сохранено первое сообщение в минуте и последнее
   Весь код выложить на GitHub. ZIP архивы не допускаются.
   Можно задавать уточняющие вопросы.
   Если вы не можете выполнить какой то из этапов то опишите причину.

----
✅ Готово

![](images/1.jpg)    
