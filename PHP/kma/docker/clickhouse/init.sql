CREATE DATABASE IF NOT EXISTS kma;

CREATE TABLE IF NOT EXISTS kma.parsers
(
    id UUID DEFAULT generateUUIDv4(),
    url String DEFAULT '',
    content_length Nullable(Int32) DEFAULT NULL,
    created_at DateTime DEFAULT now(),
    PRIMARY KEY (id)
) ENGINE = MergeTree()
      ORDER BY id;
