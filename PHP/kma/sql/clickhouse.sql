CREATE DATABASE IF NOT EXISTS kma;

CREATE TABLE IF NOT EXISTS kma.parsers
(
    id UUID DEFAULT generateUUIDv4(),
    url String DEFAULT '',
    content_length Nullable(Int32) DEFAULT NULL,
    created_at DateTime DEFAULT now(),
    PRIMARY KEY (id)
) ENGINE = MergeTree()
      ORDER BY id;

INSERT INTO kma.parsers (url, content_length, created_at)
VALUES ('https://mybox.ru/borisoglebsk', 108799, '2023-08-15 06:02:49'),
       ('https://mybox.ru/astrakhan', 108795, '2023-08-15 06:03:07'),
       ('https://mybox.ru/volgograd', 108796, '2023-08-15 06:03:16'),
       ('https://mybox.ru/efremov', 108793, '2023-08-15 06:03:25'),
       ('https://mybox.ru/domodedovo', 108796, '2023-08-15 06:03:56'),
       ('https://mybox.ru/korenovsk', 108795, '2023-08-15 06:04:01'),
       ('https://mybox.ru/elets', 108791, '2023-08-15 06:04:28'),
       ('https://mybox.ru/nizhnevartovsk', 108800, '2023-08-15 06:04:35'),
       ('https://mybox.ru/obninsk', 108793, '2023-08-15 06:04:42'),
       ('https://mybox.ru/kursk', 108792, '2023-08-15 06:05:09');


--	Вывести сколько строк за минуту, Вывести минуту группировки

SELECT
    toStartOfMinute(created_at) AS minute,
    count(*) AS row_count
FROM
    kma.parsers
GROUP BY
    minute
ORDER BY
    minute;


--Вывести среднюю длину контента

SELECT
    toStartOfMinute(created_at) AS minute,
    AVG(content_length) AS avg_content_length
FROM
    kma.parsers
GROUP BY
    minute
ORDER BY
    minute;


--Вывести когда было сохранено первое сообщение и последнее в минуте:

SELECT
    toStartOfMinute(created_at) AS minute,
    min(created_at) AS first_message_time,
    max(created_at) AS last_message_time
FROM
    kma.parsers
GROUP BY
    minute
ORDER BY
    minute;

