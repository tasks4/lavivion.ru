CREATE TABLE `parsers`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `url`            varchar(255) DEFAULT NULL,
    `content_length` int(11) DEFAULT NULL,
    `created_at`     timestamp NULL DEFAULT current_timestamp (),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

