<?php

namespace app\db;

class DatabaseConnection
{
    private static $instances = [];

    private $connection;

    private $type;
    private $host;
    private $port;
    private $username;
    private $password;
    private $database;

    /**
     * @param $host
     * @param $username
     * @param $password
     * @param $database
     */
    protected function __construct($type, $host, $port, $username, $password, $database)
    {
        $this->type = $type;
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;

        $dsn = "$this->type:host={$this->host}; port=$port; dbname={$this->database}";

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ];

        $this->connection = new \PDO($dsn, $this->username, $this->password, $options);
//        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param $host
     * @param $username
     * @param $password
     * @param $database
     * @return DatabaseConnection|mixed
     */
    public static function getInstance($type, $host, $port, $username, $password, $database): self
    {
        $key = "{$host}_{$database}";

        if (!array_key_exists($key, self::$instances)) {
            self::$instances[$key] = new static($type, $host, $port, $username, $password, $database);
        }

        return self::$instances[$key];
    }

    /**
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->connection;
    }

    /**
     * @return void
     */
    protected function __clone()
    {
    }

    /**
     * @return void
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize");
    }
}
