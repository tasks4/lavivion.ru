<?php

namespace app\jobs;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;

class ParserJob
{
    /**
     * @param $channel
     * @param array $urls
     * @return void
     */
    public static function execute(AMQPChannel $channel, array $urls, $key)
    {
        $channel->queue_declare($key, false, true, false, false);

        foreach ($urls as $url) {
            $message = new AMQPMessage($url);
            $channel->basic_publish($message, '', $key);

            echo "Sent URL: $url\n";
        }
    }
}
