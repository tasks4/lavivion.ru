<?php

namespace app\models;

class ParsersClickHouse
{
    /**
     * @var string
     */
    private $host;

    public function __construct()
    {
        $this->host = sprintf('http://%s:%s', $_ENV['CLICKHOUSE_HOST'], $_ENV['CLICKHOUSE_PORT']);
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'kma.parsers';
    }

    /**
     * @return string
     */
    public static function sqlQuantityLinesMinute(): string
    {
        $table = self::tableName();

        return <<<SQL
    SELECT
        toStartOfMinute(created_at) AS minute,
        count(*) AS row_count
    FROM
        $table
    GROUP BY
        minute
    ORDER BY
        minute
SQL;
    }

    /**
     * @return string
     */
    public static function sqlFirstAndLastMessageTimestamp(): string
    {
        $table = self::tableName();

        return <<<SQL
    SELECT
        toStartOfMinute(created_at) AS minute,
        min(created_at) AS first_message_time,
        max(created_at) AS last_message_time
    FROM
       $table
    GROUP BY
        minute
    ORDER BY
        minute
SQL;
    }

    public static function sqlAverageContentLength(): string
    {
        $table = self::tableName();

        return <<<SQL
    SELECT
        toStartOfMinute(created_at) AS minute,
        AVG(content_length) AS avg_content_length
    FROM
       $table
    GROUP BY
        minute
    ORDER BY
        minute;
SQL;
    }

    public static function sqlInsert($url, $content_length): string
    {
        $table = self::tableName();

        return <<<SQL
    INSERT INTO $table (url, content_length) VALUES ('$url', $content_length);
SQL;
    }


    /**
     * @param string $query
     * @param string $format
     * @return mixed
     */
    public function makeRequest(string $query, string $format = 'JSONCompact', $method = 'GET')
    {
        $url =
            sprintf(
                '%s?query=%s&default_format=%s',
                $this->host,
                urlencode(trim(str_replace(["\n", "\r",], '', $query))),
                $format
            );

        if ($method === 'GET') {
            return file_get_contents($url);
        }

        $options = [
            'http' => [
                'method' => $method,
                'header' => "Accept: application/json\r\nContent-Length: 0",
            ]
        ];
        $context = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }
}
