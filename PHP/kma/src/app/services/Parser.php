<?php

namespace app\services;

use app\db\DatabaseConnection;
use app\models\ParsersClickHouse;
use PhpAmqpLib\Channel\AMQPChannel;

class Parser implements ServiceInterface
{

    /**
     * @var \PDO
     */
    private $db;
    /**
     * @var ParsersClickHouse|null
     */
    private $clickHouse;

    public function __construct(\PDO $db, ParsersClickHouse $clickHouse = null)
    {
        $this->db = $db;
        $this->clickHouse = $clickHouse;
    }

    /**
     * @param AMQPChannel $channel
     * @return void
     */
    public function run(AMQPChannel $channel, $queue): void
    {
        [$queueName, $messageCount] = $channel->queue_declare($queue, true);
        $db = $this->db;
        $clickHouse = $this->clickHouse;
        $taskCount = 0;

        $callback = function ($msg) use ($db, $clickHouse) {
            $url = $msg->body;

            $content = str_replace(["\r", "\n"], '', file_get_contents($url));

            $contentLength = strlen($content);
            $sql = "INSERT INTO parsers (url, content_length) VALUES (:url, :content_length)";

            $query = $db->prepare($sql);
            $query->bindParam(":url", $url);
            $query->bindParam(":content_length", $contentLength);

            if ($query->execute()) {
                echo "Save MariaDB: $url\n";
            }

            if ($clickHouse) {
                $clickHouse->makeRequest(
                    ParsersClickHouse::sqlInsert($url, $contentLength), 'JSONCompact', 'POST'
                );

                echo "Save ClickHouse: $url\n";
            }
        };

//        $callback =    [self::class, 'save'];
//        call_user_func($callback, '', $this);

        $channel->basic_consume(
            $queue,
            '',
            false,
            true,
            false,
            false,
//            [self::class, 'save']
            $callback
        );

        while ($channel->is_consuming()) {
            $channel->wait();

            if (++$taskCount >= $messageCount){
                $channel->close();
            }
        }
    }

    /**
     * это функция остовил для теста, чтобы потом посмотреть как можно передать пораметер во время callback визова
     * @param $msg
     * @return void
     */
    public function save($msg)
    {
        $url = $msg->body;

//        $content = preg_replace("~\n||\r~sui", '', file_get_contents($url));
        $content = str_replace(["\r", "\n"], '', file_get_contents($url));
        $contentLength = strlen($content);

        $sql = "INSERT INTO parsers (url, content_length) VALUES (:url, :content_length)";
        $query = $this->db->prepare($sql);
        $query->bindParam(":url", $url);
        $query->bindParam(":content_length", $contentLength);
        $query->execute();
    }
}
