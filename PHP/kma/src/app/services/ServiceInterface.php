<?php

namespace app\services;

use PhpAmqpLib\Channel\AMQPChannel;

interface ServiceInterface
{
    public function run(AMQPChannel $channel, $queue): void;

}
