<?php

return
    \app\db\DatabaseConnection::getInstance(
       $_ENV['CLICKHOUSE_CONNECTION'],
        $_ENV['CLICKHOUSE_HOST'],
        $_ENV['CLICKHOUSE_PORT'],
        $_ENV['CLICKHOUSE_USER'],
        $_ENV['CLICKHOUSE_PASSWORD'],
        $_ENV['CLICKHOUSE_DB'],
    )->getConnection();

