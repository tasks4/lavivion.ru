<?php

return
    \app\db\DatabaseConnection::getInstance(
        $_ENV['MYSQL_CONNECTION'],
        $_ENV['MYSQL_HOST'],
        $_ENV['MYSQL_PORT'],
        $_ENV['MYSQL_USER'],
        $_ENV['MYSQL_PASSWORD'],
        $_ENV['MYSQL_DATABASE'],
    )->getConnection();

