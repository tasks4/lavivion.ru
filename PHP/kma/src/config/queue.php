<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;

return new AMQPStreamConnection('rabbitmq', $_ENV['RABBITMQ_DEFAULT_PORT'], $_ENV['RABBITMQ_DEFAULT_USER'], $_ENV['RABBITMQ_DEFAULT_PASS']);

