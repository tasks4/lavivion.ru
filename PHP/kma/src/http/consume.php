<?php
/**
 * @var $db \app\db\DatabaseConnection
 * @var $clickHouse \app\db\DatabaseConnection
 * @var $queue PhpAmqpLib\Connection\AMQPStreamConnection;
 */

require '../vendor/autoload.php';
Dotenv\Dotenv::createImmutable(__DIR__ . '/../')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'mariadb.env')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'clickhouse.env')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'rabbitmq.env')->load();

$parses = new \app\models\ParsersClickHouse();
$db = require 'config/db.php';
$queue = require 'config/queue.php';
$urls = require 'config/urls.php';

$channel = $queue->channel();

(new \app\services\Parser($db, $parses))->run($channel, 'url_queue');

$channel->close();
$queue->close();

