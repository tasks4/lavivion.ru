<?php
/**
 * @var $db \app\db\DatabaseConnection
 * @var $clickHouse \app\db\DatabaseConnection
 * @var $queue PhpAmqpLib\Connection\AMQPStreamConnection;
 */

require '../vendor/autoload.php';
Dotenv\Dotenv::createImmutable(__DIR__ . '/../')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'mariadb.env')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'clickhouse.env')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'rabbitmq.env')->load();

use app\models\ParsersClickHouse;

$parser = new ParsersClickHouse();
$db = require 'config/db.php';
$queue = require 'config/queue.php';
$urls = require 'config/urls.php';

$channel = $queue->channel();

\app\jobs\ParserJob::execute($channel, $urls, 'url_queue');

(new \app\services\Parser($db, $parser))->run($channel, 'url_queue');

$channel->close();
$queue->close();

echo PHP_EOL . 'Сколько строк за минуту, Вывести минуту группировки';
echo PHP_EOL . ($parser->makeRequest(ParsersClickHouse::sqlQuantityLinesMinute(), 'Pretty') ?? '');

echo PHP_EOL . 'Среднюю длину контента';
echo PHP_EOL . ($parser->makeRequest(ParsersClickHouse::sqlAverageContentLength(), 'Pretty') ?? '');

echo PHP_EOL . 'Ккогда было сохранено первое сообщение и последнее в минуте';
echo PHP_EOL . ($parser->makeRequest(ParsersClickHouse::sqlFirstAndLastMessageTimestamp(), 'Pretty') ?? '');


