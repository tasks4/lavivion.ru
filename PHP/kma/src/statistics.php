<?php

require '../vendor/autoload.php';

Dotenv\Dotenv::createImmutable(__DIR__ . '/../')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'mariadb.env')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'clickhouse.env')->load();
Dotenv\Dotenv::createImmutable(__DIR__ . '/../docker/envs/', 'rabbitmq.env')->load();

use app\models\ParsersClickHouse;

$parser = new ParsersClickHouse();

echo PHP_EOL . 'Сколько строк за минуту, Вывести минуту группировки';
echo PHP_EOL . ($parser->makeRequest(ParsersClickHouse::sqlQuantityLinesMinute(), 'Pretty') ?? '');

echo PHP_EOL . 'Среднюю длину контента';
echo PHP_EOL . ($parser->makeRequest(ParsersClickHouse::sqlAverageContentLength(), 'Pretty') ?? '');

echo PHP_EOL . 'Ккогда было сохранено первое сообщение и последнее в минуте';
echo PHP_EOL . ($parser->makeRequest(ParsersClickHouse::sqlFirstAndLastMessageTimestamp(), 'Pretty') ?? '');
