SELECT gorod,
       trim(
               REGEXP_REPLACE(
                       REGEXP_REPLACE(gorod,
                                      'поселок городского типа|городской поселок|сельский поселок|рабочий поселок|дачный поселок|коттеджный поселок|курортный поселок|железнодорожная станция|\yслобода\y|поселок сельского типа|поселок станции|садовое товарищество|поселок при железнодорожной станции|поселок при станции|\yпоселок\y|\yгород\y|(?<!Красное\s*)\yсело\y|\yстаница\y|\yстанция\y|\yдеревня\y|\yхутор\y|\yаул\y|\yг\y',
                                      CASE
                                          WHEN gorod ~* 'поселок городского типа' THEN 'пгт'
                                          WHEN gorod ~* 'городской поселок ' THEN 'гп'
                                          WHEN gorod ~* 'дачный поселок' THEN 'дп'
                                          WHEN gorod ~* 'сельский поселок' THEN 'сп'
                                          WHEN gorod ~* 'рабочий поселок' THEN 'рп'
                                          WHEN gorod ~* 'коттеджный поселок' THEN 'кп'
                                          WHEN gorod ~* 'курортный поселок' THEN 'кп'
                                          WHEN gorod ~* 'железнодорожная станция' THEN 'ж/д_ст'
                                          WHEN gorod ~* 'поселок при железнодорожной станции' THEN 'п/ст'
                                          WHEN gorod ~* 'поселок при станции' THEN 'п/ст'
                                          WHEN gorod ~* 'поселок сельского типа' THEN 'п'
                                          WHEN gorod ~* 'поселок станции' THEN 'п. ст'
                                          WHEN gorod ~* 'садовое товарищество' THEN 'снт'
                                          WHEN gorod ~* '\yслобода\y' THEN 'сл '
                                          WHEN gorod ~* '\yаул\y' THEN 'аул'
                                          WHEN gorod ~* '\yпоселок\y' THEN 'п '
                                          WHEN gorod ~* '\yгород\y|\yг\y' THEN ''
                                          WHEN gorod ~* '\yсело\y' THEN 'с '
                                          WHEN gorod ~* '\yстаница\y' THEN 'ст-ца '
                                          WHEN gorod ~* '\yстанция\y' THEN 'ст'
                                          WHEN gorod ~* '\yдеревня\y' THEN 'д '
                                          WHEN gorod ~* '\yхутор\y' THEN 'х '

                                          ELSE gorod
                                          end, 'i'),
                       '^(пгт|снт|аул|гп|рп|сп|дп|кп|ст|п. ст|ст\-ца|ж\/д\_ст|п\/ст|сл|с|п|д|х) (.*)',
                       '\2 \1')
           ) AS gorod1
FROM (SELECT REPLACE((string_to_array(address, ','))[1], 'ё', 'е') AS gorod,
             COUNT(*) AS total,
             SUM((
                             nalichie_28_50 = 'В наличии' OR
                             nalichie_28_100 = 'В наличии' OR
                             nalichie_28_200 = 'В наличии' OR
                             nalichie_56_50 = 'В наличии' OR
                             nalichie_56_100 = 'В наличии' OR
                             nalichie_56_200 = 'В наличии') ::INT
                 ) AS has
      FROM placemarks_layer_data.placemarks_layer_data_34707
      GROUP BY gorod
      order by gorod) t;


-- Или можно так реалиовать
WITH normalized_gorods AS (SELECT REPLACE((string_to_array(address, ','))[1], 'ё', 'е') AS gorod, COUNT(*) AS total,
                                  SUM((
                                                  nalichie_28_50 = 'В наличии' OR
                                                  nalichie_28_100 = 'В наличии' OR
                                                  nalichie_28_200 = 'В наличии' OR
                                                  nalichie_56_50 = 'В наличии' OR
                                                  nalichie_56_100 = 'В наличии' OR
                                                  nalichie_56_200 = 'В наличии') ::INT
                                      ) AS has
                           FROM placemarks_layer_data.placemarks_layer_data_34707
                           GROUP BY gorod
                           )
SELECT gorod,
       trim(
               REGEXP_REPLACE(
                       REGEXP_REPLACE(gorod,
                                      'поселок городского типа|городской поселок|сельский поселок|рабочий поселок|дачный поселок|коттеджный поселок|курортный поселок|железнодорожная станция|\yслобода\y|поселок сельского типа|поселок станции|садовое товарищество|поселок при железнодорожной станции|поселок при станции|\yпоселок\y|\yгород\y|(?<!Красное\s*)\yсело\y|\yстаница\y|\yстанция\y|\yдеревня\y|\yхутор\y|\yаул\y|\yг\y',
                                      CASE
                                          WHEN gorod ~* 'поселок городского типа' THEN 'пгт'
                                          WHEN gorod ~* 'городской поселок ' THEN 'гп'
                                          WHEN gorod ~* 'дачный поселок' THEN 'дп'
                                          WHEN gorod ~* 'сельский поселок' THEN 'сп'
                                          WHEN gorod ~* 'рабочий поселок' THEN 'рп'
                                          WHEN gorod ~* 'коттеджный поселок' THEN 'кп'
                                          WHEN gorod ~* 'курортный поселок' THEN 'кп'
                                          WHEN gorod ~* 'железнодорожная станция' THEN 'ж/д_ст'
                                          WHEN gorod ~* 'поселок при железнодорожной станции' THEN 'п/ст'
                                          WHEN gorod ~* 'поселок при станции' THEN 'п/ст'
                                          WHEN gorod ~* 'поселок сельского типа' THEN 'п'
                                          WHEN gorod ~* 'поселок станции' THEN 'п. ст'
                                          WHEN gorod ~* 'садовое товарищество' THEN 'снт'
                                          WHEN gorod ~* '\yслобода\y' THEN 'сл '
                                          WHEN gorod ~* '\yаул\y' THEN 'аул'
                                          WHEN gorod ~* '\yпоселок\y' THEN 'п '
                                          WHEN gorod ~* '\yгород\y|\yг\y' THEN ''
                                          WHEN gorod ~* '\yсело\y' THEN 'с '
                                          WHEN gorod ~* '\yстаница\y' THEN 'ст-ца '
                                          WHEN gorod ~* '\yстанция\y' THEN 'ст'
                                          WHEN gorod ~* '\yдеревня\y' THEN 'д '
                                          WHEN gorod ~* '\yхутор\y' THEN 'х '

                                          ELSE gorod
                                          end, 'i'),
                       '^(пгт|снт|аул|гп|рп|сп|дп|кп|ст|п. ст|ст\-ца|ж\/д\_ст|п\/ст|сл|с|п|д|х) (.*)',
                       '\2 \1')
           ) AS gorod1
FROM normalized_gorods
ORDER BY gorod;
