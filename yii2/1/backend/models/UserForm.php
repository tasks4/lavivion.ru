<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\web\NotFoundHttpException;

class UserForm extends User
{
    /**
     * @var
     */
    public $username;


    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $password;
    /**
     * @var
     */
    public $status;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['status', 'required'],
            [['password',], 'required', 'on'=>'create'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
        ];
    }

    /**
     * @return User|bool
     * @throws \yii\base\Exception
     */
    public function saveData(): User
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User([
            'username' => $this->username,
            'email' => $this->email,
            'status' => $this->status,
            'password_reset_token' => Yii::$app->security->generateRandomString(),
        ]);

        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->status != 10) {
            $user->generateEmailVerificationToken();
        }

        if (!$user->save()) {
            return false;
        }

        return $user;
    }
}
