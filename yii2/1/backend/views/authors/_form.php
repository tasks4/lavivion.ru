<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var common\models\Author $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="author-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-9">
        <?= $form->field($model, 'is_status')
            ->widget(
                \kartik\switchinput\SwitchInput::classname(),
                [
                    'value' => true,
                    'pluginOptions' =>
                        [
                            'size' => 'large',
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => Yii::t('app', 'Active'),
                            'offText' => Yii::t('app', 'Inactive'),
                        ],
                ]
            );
        ?>
    </div>

    <div class="col-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-6">
                <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'short_description')->textarea(['maxlength' => true]) ?>

            </div>
            <div class="col-6">
                <?= $form->field($model, 'seo_description')->textarea(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="col-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
