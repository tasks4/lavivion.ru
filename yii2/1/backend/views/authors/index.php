<?php

use common\models\Author;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var backend\models\AuthorSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Authors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Author'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'label' => 'Quantity of books',
                'value' => function ($model) {
                    return count($model->books ?? []);
                }
            ],
            'slug',
            'short_description',
//            'description:ntext',
            //'seo_title',
            //'seo_keywords',
            //'seo_description',
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    return \common\models\User::findOne($model->updated_by)->username;
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    return \common\models\User::findOne($model->updated_by)->username;
                }
            ],
            'created_at',
            'updated_at',
            'is_status:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Author $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
