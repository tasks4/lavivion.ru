<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%apples}}".
 *
 * @property int $id
 * @property string $color
 * @property int $appearance_date
 * @property int|null $fall_date
 * @property int $status
 * @property float $size
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property int|null $is_active
 */
class Apple extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%apples}}';
    }

    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function () {
                    return date('Y-m-d h:i:s');
                },
            ],
        ];
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!empty($this->created_by) && !empty($this->updated_by)) {
                return true;
            }

            if ($this->isNewRecord) {
                $this->created_by = Yii::$app->user->id ?? 0;
            }

            $this->updated_by = Yii::$app->user->id ?? 0;

            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color', 'appearance_date', 'status', 'size'], 'required'],
            [['appearance_date', 'fall_date', 'status', 'created_by', 'updated_by', 'is_active'], 'integer'],
            [['size'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'color' => Yii::t('app', 'Color'),
            'appearance_date' => Yii::t('app', 'Appearance Date'),
            'fall_date' => Yii::t('app', 'Fall Date'),
            'status' => Yii::t('app', 'Status'),
            'size' => Yii::t('app', 'Size'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AppleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AppleQuery(get_called_class());
    }
}
