<?php

namespace console\controllers;

use common\models\Author;
use common\models\Book;
use Faker\Factory;
use yii\console\Controller;

class GenerateController extends Controller
{
    /**
     * @return true
     */
    public function actionAuthors()
    {
        $faker = Factory::create();

        for($i = 0; $i < 100; $i++)
        {
            $model = new Author();
            $model->name = $faker->name();
            $model->seo_title =  $model->name;
            $model->seo_keywords = $faker->text(30);
            $model->seo_description = $faker->text(100);
            $model->short_description = $faker->text(100);
            $model->description = $faker->text(rand(300, 500));
            $model->created_by = 1;
            $model->updated_by = 1;
            $model->is_status = 1;
            $model->save();
        }

        return true;
    }

    /**
     * @return true
     */
    public function actionBooks()
    {
        $faker = Factory::create();

        for($i = 0; $i < 100; $i++)
        {
            $model = new Book();
            $model->title = $faker->name();
            $model->author_id =  rand(1, 99);
            $model->seo_title =  $model->title;
            $model->seo_keywords = $faker->text(30);
            $model->seo_description = $faker->text(100);
            $model->short_description = $faker->text(100);
            $model->description = $faker->text(rand(300, 500));
            $model->created_by = 1;
            $model->updated_by = 1;
            $model->is_status = 1;
            $model->save();
        }

        return true;
    }
}
