<?php

use yii\db\Migration;

/**
 * Class m230604_182558_authors
 */
class m230604_182558_authors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%authors}}', [
            'id' => $this->primaryKey(),

            'name' => $this->string(),
            'slug' => $this->string(),

            'short_description' => $this->string(),
            'description' => $this->text(),

            'seo_title' => $this->string(),
            'seo_keywords' => $this->string(),
            'seo_description' => $this->string(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',

            'is_status' => $this->tinyInteger(2),

        ], $tableOptions);

        $this->createIndex('authors_name', 'authors', 'name');
        $this->createIndex('authors_slug', 'authors', 'slug');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%authors}}');
    }
}
