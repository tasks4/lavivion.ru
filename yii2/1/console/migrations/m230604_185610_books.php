<?php

use yii\db\Migration;

/**
 * Class m230604_185610_books
 */
class m230604_185610_books extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),

            'title' => $this->string(),
            'slug' => $this->string(),
            'author_id' => $this->integer(),

            'short_description' => $this->string(),
            'description' => $this->text(),

            'seo_title' => $this->string(),
            'seo_keywords' => $this->string(),
            'seo_description' => $this->string(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',

            'is_status' => $this->tinyInteger(2),

        ], $tableOptions);

        $this->createIndex('books_title', 'books', 'title');
        $this->createIndex('books_slug', 'books', 'slug');
        $this->createIndex('books_author_id', 'books', 'author_id');

        $this->addForeignKey(
            'FK_books',
            'books',
            'author_id',
            'authors',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
