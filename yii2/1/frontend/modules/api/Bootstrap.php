<?php

namespace frontend\modules\api;

use \yii\rest\UrlRule;

class Bootstrap implements \yii\base\BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'class' => UrlRule::class,
                'controller' => [
                    'api/v1/books',
                ]
            ],
            'POST api/v1/books/list' => 'api/v1/books/list',
            'GET /api/v1/books/by-id' => 'api/v1/books/by-id',
            'POST /api/v1/books/update' => 'api/v1/books/update',
            'DELETE /api/v1/books/delete' => 'api/v1/books/delete',

        ]);
    }
}
