<?php
namespace frontend\modules\api;

use Yii;
use yii\web\Response;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\api\controllers';

    const MODULE = "api";

    public function init()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        parent::init();
    }
}
