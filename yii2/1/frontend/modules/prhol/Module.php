<?php
namespace frontend\modules\prhol;

use Yii;
use yii\console\Application;
use yii\web\Response;

class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $controllerNamespace = 'frontend\modules\prhol\controllers';

    const MODULE = "prhol";

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof  Application) {
            $this->controllerNamespace = 'frontend\modules\prhol\console\controllers';
        }
    }
}
