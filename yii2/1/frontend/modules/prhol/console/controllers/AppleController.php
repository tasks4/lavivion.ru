<?php

namespace frontend\modules\prhol\console\controllers;

use frontend\modules\prhol\models\Apple;
use yii\console\Controller;

class AppleController extends Controller
{

    public function actionIndex()
    {
        $apple =  Apple::createApple('green');

        echo $apple->color.PHP_EOL;

//        $apple->eat(50); // Бросить исключение - Съесть нельзя, яблоко на дереве
        echo $apple->size.PHP_EOL;

        $apple->fallToGround();
        $apple->eat(25);
        echo $apple->size.PHP_EOL;


    }


}
