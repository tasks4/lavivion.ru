<?php

namespace frontend\modules\prhol\console\migrations;


use yii\db\Migration;

/**
 * Handles the creation of table `{{%apple}}`.
 */
class m230716_221645_create_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apples}}', [
            'id' => $this->primaryKey(),
            'color' => $this->string()->notNull(),
            'appearance_date' => $this->integer()->notNull(),
            'fall_date' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
            'size' => $this->decimal(5, 2)->notNull(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',

            'is_active' => $this->tinyInteger(2)->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apples}}');
    }
}
