<?php

namespace frontend\modules\prhol\models;

class Apple extends \common\models\Apple
{
    const STATUS_ON_TREE = 0;
    const STATUS_ON_GROUND = 1;
    const STATUS_ROTTEN = 2;

    /**
     * @param $percent
     * @return void
     * @throws \Exception
     */
    public function eat($percent)
    {
        if ($this->status === self::STATUS_ON_TREE) {
            throw new \Exception('Съесть нельзя, яблоко на дереве');
        }

        if ($this->status === self::STATUS_ROTTEN) {
            throw new \Exception('Яблоко испорчено');
        }

        $this->size -= ($percent / 100);

        if ($this->size <= 0 || $this->isRotten()) {
            $this->size = 0;
            $this->status = self::STATUS_ROTTEN;

            $this->deleteApple();
        }

        $this->save();
    }

    /**
     * @return void
     */
    public function fallToGround()
    {
        if ($this->status === self::STATUS_ON_TREE) {
            $this->status = self::STATUS_ON_GROUND;
            $this->fall_date = time();
            $this->save();
        }
    }

    /**
     * @return bool
     */
    public function isRotten()
    {
        if ($this->status === self::STATUS_ON_GROUND) {
            $currentTimestamp = time();
            $rottenTimestamp = $this->fall_date + 5 * 60 * 60;
            return $currentTimestamp >= $rottenTimestamp;
        }

        return false;
    }

    /**
     * @return void
     */
    public function deleteApple()
    {
        if ($this->status === self::STATUS_ROTTEN) {
            $this->is_active = 0;
            $this->save();
        }
    }

    /**
     * @param $color
     * @return Apple|self|null
     */
    public static function createApple($color)
    {
        $apple = self::findOne(['color' => $color, 'is_active' => 1]);

        if ($apple) {
            return $apple;
        }

        $apple = new self();
        $apple->size = 1;
        $apple->color = $color;
        $apple->appearance_date = time();
        $apple->status = self::STATUS_ON_TREE;

        return $apple;
    }
}
